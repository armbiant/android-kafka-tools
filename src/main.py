#!/usr/bin/python3

import yaml
import subprocess
import argparse


def read_config(config_file_path):
    with open(config_file_path, 'r') as f:
        return yaml.safe_load(f)


def create_connection_config(login, password):
    with open('/tmp/config.properties', 'w+') as f:
        f.write(
            f"""sasl.jaas.config=org.apache.kafka.common.security.plain.PlainLoginModule required username="{login}" password="{password}";
security.protocol=SASL_PLAINTEXT
sasl.mechanism=PLAIN
""")


def create_topic(bootstrap_server, name, options):
    command = [
        'kafka-topics.sh',
        '--bootstrap-server', bootstrap_server,
        '--command-config', '/tmp/config.properties',
        '--create',
        '--if-not-exists',
        '--topic', name
    ]

    if options["partitions"]:
        command.extend(['--partitions=%s' % options["partitions"]])
    if options["replicas"]:
        command.extend(['--replication-factor=%s' % options["replicas"]])
    for item in options["config"]:
        key = item["name"]
        value = item["value"]
        command.extend(['--config', f"{key}={value}"])

    print('Run: %s' % ' '.join(command))
    subprocess.run(command)


def read_names_from_file(filename):
    result = []
    with open(filename, 'r') as f:
        for line in f:
            name = line.strip()
            if name != "":
                result.append(name)

    return result


def get_all_topic_names(topic_configs_list):
    result = []
    for one_topic_config in topic_configs_list["topics"]:
        result.append(one_topic_config["name"])

    return result


def get_desired_topic_names(args, topics_config):
    result = []
    if args.topic_name is not None:
        if args.topic_name == 'all':
            result = get_all_topic_names(topics_config)
        else:
            result = [args.topic_name]
    elif args.topic_names_file is not None:
        result = read_names_from_file(args.topic_names_file)
    else:
        print("Either --topic-name or --topic-names-file option is required")
        exit(1)

    if len(result) == 0:
        print("No topics for create")
        exit(0)

    return result


def create_topics(desired_topic_names, topics_config):
    for topic_options in topics_config["topics"]:
        if topic_options["name"] in desired_topic_names:
            create_topic(args.bootstrap_server, topic_options["name"], topic_options)


def check_all_configs_exists(desired_topic_names, topics_config):
    configured_topic_names = map(lambda topic: topic["name"], topics_config["topics"])
    for desired_topic in desired_topic_names:
        if desired_topic not in configured_topic_names:
            print(f"No config for topic {desired_topic}")
            exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create topics from file')
    parser.add_argument('--bootstrap-server', dest='bootstrap_server', required=True)
    parser.add_argument('--topics-file', dest='topics_file', required=True)
    parser.add_argument('--kafka-login', dest='kafka_login', required=True)
    parser.add_argument('--kafka-password', dest='kafka_password', required=True)
    parser.add_argument('--topic-name', dest='topic_name')
    parser.add_argument('--topic-names-file', dest='topic_names_file')

    args = parser.parse_args()

    topics_config = read_config(args.topics_file)
    desired_topic_names = get_desired_topic_names(args, topics_config)
    check_all_configs_exists(desired_topic_names, topics_config)
    create_connection_config(args.kafka_login, args.kafka_password)
    create_topics(desired_topic_names, topics_config)

    print('Done')
